﻿using UnityEngine;
using System.Collections;

public class SpringObstacle : MonoBehaviour {

	/*
	private float startAngle = 0;
	private float startTouchAngle = 0;
	*/
	// Use this for initialization
	void Start () {
		InputManager.main.SubscribeToOnTouchDown(TouchDown);
		InputManager.main.SubscribeToOnTouchHold(TouchHold);
		InputManager.main.SubscribeToOnTouchUp(TouchUp);
	}
	
	void TouchDown(GameObject _go, Vector3 _point)
	{
		if (_go == this.gameObject)
		{
			/*
			startAngle = transform.localEulerAngles.z;
			startTouchAngle = Vector3.Angle(Vector3.right, new Vector3(_point.x - transform.position.x, _point.y - transform.position.y, transform.position.z));    
			
			if (_point.y < transform.position.y)
			{
				startTouchAngle = -startTouchAngle;
			}
			*/
		}
	}
	
	void TouchHold(GameObject _go, Vector3 _inputStart, Vector3 _point, int _fingerID)
	{

		if (_go == this.gameObject)
		{
			transform.position = new Vector3(_point.x, _point.y, transform.position.z);
			/*
			float curTouchAngle = Vector3.Angle(Vector3.right, new Vector3(_point.x - transform.position.x, _point.y - transform.position.y, transform.position.z));
			
			if (_point.y < transform.position.y)
			{
				curTouchAngle = -curTouchAngle;
			}
			float rotAngle = startAngle+ (curTouchAngle - startTouchAngle);
			//Debug.Log("start angle: " + startAngle + " start touch angle: "+ startTouchAngle+ " curTouchAngle: " + curTouchAngle + " rotAngle: " + rotAngle);
			
			//if (transform.localEulerAngles.z > 200 && transform.localEulerAngles.z < 220 ) return;
			transform.localEulerAngles = new Vector3(0, 0, rotAngle);
			*/
		}
	}

	void TouchUp(GameObject _go, Vector3 _inputStart, Vector3 _point, int _fingerID)
	{
		
	}

}
