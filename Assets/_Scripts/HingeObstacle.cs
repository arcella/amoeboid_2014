﻿using UnityEngine;
using System.Collections;

public class HingeObstacle : MonoBehaviour {

	private float startAngle = 0;
	private float startTouchAngle = 0;

	private float startingRot;
	private HingeJoint myHinge;

	private bool canDrag = true;

	private Vector3 _curInputPoint;
	// Use this for initialization
	void Start () {

		startingRot = transform.localEulerAngles.z;
		myHinge = gameObject.GetComponent<HingeJoint>();
		InputManager.main.SubscribeToOnTouchDown(TouchDown);
		InputManager.main.SubscribeToOnTouchHold(TouchHold);
		InputManager.main.SubscribeToOnTouchUp(TouchUp);
	}


	void TouchDown(GameObject _go, Vector3 _point)
	{
		if (_go == this.gameObject)
		{
			canDrag = true;

			startAngle = transform.localEulerAngles.z;
			startTouchAngle = Vector3.Angle(Vector3.right, new Vector3(_point.x - transform.position.x, _point.y - transform.position.y, transform.position.z));    

			if (_point.y < transform.position.y)
			{
				startTouchAngle = -startTouchAngle;
			}	
	
		}
	}
	
	void TouchHold(GameObject _go, Vector3 _inputStart, Vector3 _point, int _fingerID)
	{
		if (InputManager.main.activeObject[_fingerID.ToString()] == this.gameObject)
		{
			if (canDrag)
			{
				_curInputPoint = _point;
		
				float curTouchAngle = Vector3.Angle(Vector3.right, new Vector3(_point.x - transform.position.x, _point.y - transform.position.y, transform.position.z));

				if (_point.y < transform.position.y)
				{
					curTouchAngle = -curTouchAngle;
				}
				float rotAngle = startAngle+(curTouchAngle - startTouchAngle);
				//Debug.Log("start angle: " + startAngle + " start touch angle: "+ startTouchAngle+ " curTouchAngle: " + curTouchAngle + " rotAngle: " + rotAngle);
				//Debug.DrawLine(new Vector3(0, 0, curTouchAngle), new Vector3(0, 0, startTouchAngle));
				
				//if (transform.localEulerAngles.z > 200 && transform.localEulerAngles.z < 220 ) return;
				/*
				if (startAngle - rotAngle > 180)
				{
					rotAngle = 360 - rotAngle;
				}
				*/

				if (rotAngle <= startingRot + myHinge.limits.max && rotAngle >= startingRot + myHinge.limits.min)
				{
					transform.localEulerAngles = Vector3.Lerp(transform.localEulerAngles, new Vector3(0, 0, rotAngle), Time.time*.01f);
				}
				else 
					canDrag = false;
			}
		}
	}


	void TouchUp(GameObject _go, Vector3 _inputStart, Vector3 _point, int _fingerID)
	{
		if (_go == this.gameObject)
		{
			canDrag = false;
		}
	}
}
