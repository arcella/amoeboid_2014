using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {
       

	static public AudioManager main;
    [System.NonSerialized] public bool isPlayingMusic = true; 
    private enum Fade {In, Out};
    
    private AudioClip musicTrack;
	
	
	
   void Awake()
	{
		main = this;
	}
    
    // For SFX and ambient sound
    public void PlaySound(AudioClip _clip, bool _loopBool) 
    {
		GameObject audioObj = Instantiate(Resources.Load("AudioObject")) as GameObject;
		AudioSource audioSource = audioObj.GetComponent<AudioSource>();
		audioObj.name = _clip.name;
        audioSource.clip = _clip;
    
		//Set to false if no loop is required
        audioSource.loop = _loopBool;

        audioSource.Play();
        
		if (!_loopBool)
		{
	        //Destroy the audio clip when it has finished playing
	        Destroy(audioObj, _clip.length);
		}
    }
	
	public void PlaySoundWithDelay(AudioClip _clip, float _delay, bool _loopBool)
	{
		StartCoroutine(AudioDelayPlay(_clip, _delay, _loopBool));
	}
	
	IEnumerator AudioDelayPlay(AudioClip _clip, float _delay, bool _loopBool)
	{
		yield return new WaitForSeconds(_delay);
		
		GameObject audioObj = Instantiate(Resources.Load("AudioObject")) as GameObject;
		AudioSource audioSource = audioObj.GetComponent<AudioSource>();
		audioObj.name = _clip.name;
        audioSource.clip = _clip;
    
		//Set to false if no loop is required
        audioSource.loop = _loopBool;

        audioSource.Play();
        
		if (!_loopBool)
		{
	        //Destroy the audio clip when it has finished playing
	        Destroy(audioObj, _clip.length);
		}
	}

    public void PlayFadeIn(AudioClip _clip, bool _loopBool, float _fadeTime) 
    {
       // print("PlayMusic : " + clip.name);

        GameObject audioObj = Instantiate(Resources.Load("AudioObject")) as GameObject;
		AudioSource audioSource = audioObj.GetComponent<AudioSource>();
		audioObj.name = _clip.name;
        audioSource.clip = _clip;
        
        //Set to false if no loop is required
        audioSource.loop = _loopBool;
        
        //Fader, set fade time to 0 if no fade is required
        StartCoroutine(FadeAudio(audioSource, _fadeTime, Fade.In));
        audioSource.Play();
    }
    
    
    public void DestroyFadeOut(GameObject _audioObj, float _fadeTime) 
    {
		//HACK
		if (_audioObj != null)
		{
        //isPlayingMusic = false;
  		print(_audioObj.name + " !!!");
        AudioSource audioSource = _audioObj.GetComponent<AudioSource>();
		
        //Fader, set fade time to 0 if no fade is required
        StartCoroutine(FadeAudio(audioSource, _fadeTime, Fade.Out));
		}
    }
	
	public void DestroySound(GameObject _audioObj)
	{
		Destroy(_audioObj);
	}
    
    IEnumerator FadeAudio(AudioSource _audioSource, float _timer, Fade _fadeType) 
    {
    
        float start = _fadeType == Fade.In? 0.0F : 1.0F;
        float end = _fadeType == Fade.In? 1.0F : 0.0F;
        float i = 0.0F;
        float step = 1.0F/_timer;
    
        while (i <= 1.0F) {
            i += step * Time.deltaTime;
            if (_audioSource != null) _audioSource.volume = Mathf.Lerp(start, end, i);
            yield return new WaitForSeconds(step * Time.deltaTime);
        }
        
        if (_fadeType == Fade.Out) {
            if (_audioSource != null) Destroy(_audioSource.gameObject);
            print ("destroy audio");
        }

    }
    
    public void LoadAndPlay(string _soundPath) 
    {
        print ("Load and Play "+_soundPath);
        AudioClip sound = Resources.Load(_soundPath) as AudioClip;
        print (sound);
        PlaySound(sound, false);
    }
    
    public void LoadAndPlayLoop(string _soundPath) 
    {
        print ("Load and Play "+_soundPath);
        musicTrack = Resources.Load(_soundPath) as AudioClip;
        //PlaySound(musicTrack, Camera.main.transform.position, 0f, true);
        PlayFadeIn(musicTrack, true, 0f);
    }
}

