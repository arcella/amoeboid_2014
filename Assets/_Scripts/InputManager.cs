﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager: MonoBehaviour {


	static public InputManager main;

	// TODO This needs to be generalized so that it's not specific to one object
	[System.NonSerialized]
	public bool isHoldingHero = false;

	[System.NonSerializedAttribute] public Dictionary<string, GameObject> activeObject = new Dictionary<string, GameObject>();
	[System.NonSerializedAttribute] public Dictionary<string, Vector3> startingInputPos = new Dictionary<string, Vector3>();
	[System.NonSerializedAttribute] public Dictionary<string, int> tapCount = new Dictionary<string, int>();
	[System.NonSerializedAttribute] public Dictionary<string, float> prevTapUpTimeStamp = new Dictionary<string, float>();
	[System.NonSerializedAttribute] public Dictionary<string, float> thisTapDownTimeStamp = new Dictionary<string, float>();
	[System.NonSerializedAttribute] public Dictionary<string, float> currentDownTouchTimeStamp = new Dictionary<string, float>();
	
	[System.NonSerialized] public Camera raycastCam;
	public Camera raycastCam_UI;
	

	private float maxDownTouchTapTme = 1f;
	private float maxTimeBetweenTaps = 1f;

	private Dictionary<string, bool> voidStationaryHold = new Dictionary<string, bool>();
	private float resetHoldPosTimer = .5f; 
	private Dictionary<string, float> resetHoldPosTimeVal = new Dictionary<string, float>();
	private Dictionary<string, Vector3> stationaryInputPos = new Dictionary<string, Vector3>(); //Vector3.zero;

	private float shakeThreshHold = 1.25f;
	private float shakeTimeVar = 0;

	public LayerMask worldMask;

	//private UILabel mobileDebug;

	void Awake()
	{
		main = this;
	}
	public void Start()
	{
		raycastCam = Camera.main;

		for (int i = 0; i < 10; i++)
		{
			activeObject.Add(i.ToString(), null);
			startingInputPos.Add(i.ToString(), Vector3.zero);
			tapCount.Add(i.ToString(), 0);
			prevTapUpTimeStamp.Add(i.ToString(), 0);
			thisTapDownTimeStamp.Add(i.ToString(), 0);
			currentDownTouchTimeStamp.Add(i.ToString(), 0);

			resetHoldPosTimeVal.Add(i.ToString(), 0);
			voidStationaryHold.Add(i.ToString(), false);
			stationaryInputPos.Add(i.ToString(), Vector3.zero);
		}
	}
	
	void Update()
	{
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {

			for (int i = 0; i < Input.touches.Length; i++) {
				
				if (Input.touches[i].phase == TouchPhase.Ended)
				{
					onTouchUp(Input.touches[i].fingerId, Input.touches[i].position.x, Input.touches[i].position.y);
				}
				else if (Input.touches[i].phase == TouchPhase.Began)
				{
					onTouchDown(Input.touches[i].fingerId, Input.touches[i].position.x, Input.touches[i].position.y);
				}
				else if (Input.touches[i].phase == TouchPhase.Stationary ||
				         (Input.touches[i].phase == TouchPhase.Moved))
				{
					onTouchHold(Input.touches[i].fingerId, Input.touches[i].position.x, Input.touches[i].position.y);
				}
				else // touch cancelled
				{
					onTouchCancelled(Input.touches[i].fingerId, Input.touches[i].position.x, Input.touches[i].position.y);
				}
			}
		}
		
		else {
			
			if (Input.GetMouseButtonDown(0)) {
				onTouchDown(0, Input.mousePosition.x, Input.mousePosition.y);
			}
			if (Input.GetMouseButton(0)) {
				onTouchHold(0, Input.mousePosition.x, Input.mousePosition.y);
			}
			if (Input.GetMouseButtonUp(0)) {
				onTouchUp(0, Input.mousePosition.x, Input.mousePosition.y);
			}
		}

		//if (Input.acceleration.sqrMagnitude > shakeThreshHold || Input.acceleration.sqrMagnitude < -shakeThreshHold)
		if (Input.acceleration.y < -shakeThreshHold || Input.acceleration.y > shakeThreshHold || Input.acceleration.x < -shakeThreshHold || Input.acceleration.x > shakeThreshHold)
		{
			RegisterShake(Input.acceleration.x, Input.acceleration.y);
			if (canRegisterShake)
			{
				canRegisterShake = false;
				shakeTimeVar = Time.time;
			}
		}

		if (Time.time - shakeTimeVar > 2f)
		{
			canRegisterShake = true;
		}

		if (prevTapUpTimeStamp.Count > 0)
		{
			for (int j = 0; j < 10; j++)
			{
				if (Time.time - prevTapUpTimeStamp[j.ToString()] > maxTimeBetweenTaps)
					tapCount[j.ToString()] = 0;
			}
		}

	}
	
	void onTouchDown(int fingerID, float mouseX, float mouseY) {

		Vector3 currentMousePos = new Vector3(mouseX, mouseY);
		//currentMousePos
		if (raycastCam != null)
		{
			Ray ray = raycastCam.ScreenPointToRay(currentMousePos);
			//Debug.DrawLine(Camera.main.transform.position, currentMousePos);
			RaycastHit hit;
			int groundMask = 1 << 8;
			//groundMask = ~groundMask;
			if (Physics.Raycast(ray, out hit, 1000, groundMask)) {
				//m_mouseTarget = hit.point;
				thisTapDownTimeStamp[fingerID.ToString()] = Time.time;
				startingInputPos[fingerID.ToString()] = hit.point;
				activeObject[fingerID.ToString()] = hit.collider.gameObject;
				
				OnTouchDownRecognizer(hit.collider.gameObject, hit.point);
				
				currentDownTouchTimeStamp[fingerID.ToString()] = Time.time;
				resetHoldPosTimeVal[fingerID.ToString()] = Time.time;
			}
			
		}

		if (raycastCam_UI != null)
		{
			Ray ray_UI = raycastCam_UI.ScreenPointToRay(currentMousePos);
			
			RaycastHit hit_UI;
			int groundMask_UI = 1 << 12;
			if (Physics.Raycast(ray_UI, out hit_UI, 1000, groundMask_UI))
			{
				// TODO: Time based logic for this cam
				OnTouchDownRecognizer_UI(hit_UI.collider.gameObject, hit_UI.point);
			}
		}
	}

	void onTouchHold(int fingerID, float mouseX, float mouseY) {

		Vector3 currentMousePos = new Vector3(mouseX, mouseY);
		//currentMousePos

		if (raycastCam != null)
		{
		
			Ray ray = raycastCam.ScreenPointToRay(currentMousePos);
			//Debug.DrawLine(Camera.main.transform.position, currentMousePos);
			 
			RaycastHit hit;
			int groundMask = 1 << 8;
			//groundMask = ~groundMask;
			if (Physics.Raycast(ray, out hit, 1000, groundMask)) {

				if (activeObject[fingerID.ToString()] != null)
					OnTouchHoldRecognizer(activeObject[fingerID.ToString()], startingInputPos[fingerID.ToString()], new Vector3(hit.point.x, hit.point.y, 0), fingerID);

				/*
				OnTouchHoldRecognizer(hit.collider.gameObject, startingInputPos[fingerID.ToString()], hit.point, fingerID);
				RegisterStationaryHold(hit.collider.gameObject, startingInputPos[fingerID.ToString()], hit.point, fingerID);
				*/
			}
			                                     
		}
		
		if (raycastCam_UI != null)
		{
			Ray ray_UI = raycastCam_UI.ScreenPointToRay(currentMousePos);
			
			RaycastHit hit_UI;
			int groundMask_UI = 1 << 12;
			if (Physics.Raycast(ray_UI, out hit_UI, 1000, groundMask_UI))
			{
				// TODO: Time based logic for this cam
				OnTouchHoldRecognizer_UI(hit_UI.collider.gameObject, hit_UI.point);
				Debug.DrawLine(raycastCam_UI.transform.position, hit_UI.point);
			}
		}
		 	
		
	}
	
	void onTouchUp(int fingerID, float mouseX, float mouseY) {
		
		Vector3 currentMousePos = new Vector3(mouseX, mouseY);
		//currentMousePos

		if (raycastCam != null)
		{
			
			Ray ray = raycastCam.ScreenPointToRay(currentMousePos);
			//Debug.DrawLine(Camera.main.transform.position, currentMousePos);
			
			RaycastHit hit;
			int groundMask = 1 << 8;
			//groundMask = ~groundMask;
			if (Physics.Raycast(ray, out hit, 1000, groundMask)) {

				if (Vector3.Distance(hit.point, startingInputPos[fingerID.ToString()]) > 1.5f)
				{
					OnSwipeEndOnRecognizer(hit.collider.gameObject, hit.point, startingInputPos[fingerID.ToString()]);
					OnSwipeStartOnRecognizer(activeObject[fingerID.ToString()], hit.point, startingInputPos[fingerID.ToString()]);
					
				}
				
				if (Time.time - thisTapDownTimeStamp[fingerID.ToString()] < maxDownTouchTapTme) {
					if (Vector3.Distance(hit.point, startingInputPos[fingerID.ToString()]) < .25)
						RegisterTap(hit.collider.gameObject, hit.point, fingerID);
				}
				
				OnTouchUpRecognizer(activeObject[fingerID.ToString()], startingInputPos[fingerID.ToString()], hit.point, fingerID);
				voidStationaryHold[fingerID.ToString()] = false;
			}
		}


		if (raycastCam_UI != null)
		{

			Ray ray_UI = raycastCam_UI.ScreenPointToRay(currentMousePos);
			
			RaycastHit hit_UI;
			int groundMask_UI = 1 << 12;
			if (Physics.Raycast(ray_UI, out hit_UI, 1000, groundMask_UI))
			{
				// TODO: Time based logic for this cam
				OnTouchUpRecognizer_UI(hit_UI.collider.gameObject, hit_UI.point);
				
				//TODO UI SWIPE
			}
		}
	}
	
	void onTouchCancelled(int fingerID, float mouseX, float mouseY) {
		
	}

	void RegisterTap(GameObject _target, Vector3 _point, int _fingerID)
	{
		tapCount[_fingerID.ToString()]++;
		prevTapUpTimeStamp[_fingerID.ToString()] = Time.time;
		OnTapRecognizer(_target, _point);
		
		if (tapCount[_fingerID.ToString()] == 2)
		{
			// Double tap
			OnDoubleTapRecognizer(_target, _point);
			tapCount[_fingerID.ToString()] = 0;
		}
	}


	void RegisterStationaryHold (GameObject _target, Vector3 _startingInputPos, Vector3 _point, int _fingerID)
	{

		//If player has not moved pawn significantly over the past x amount of time, register on long hold
		if (Time.time - resetHoldPosTimeVal[_fingerID.ToString()] > resetHoldPosTimer)
		{
			resetHoldPosTimeVal[_fingerID.ToString()] = Time.time;
			stationaryInputPos[_fingerID.ToString()] = _point;
		}


		if (Vector3.Distance(stationaryInputPos[_fingerID.ToString()], _point) < .5f)
		{
			voidStationaryHold[_fingerID.ToString()] = false;
		}
		else
		{
			voidStationaryHold[_fingerID.ToString()] = true;
		}

		OnStationaryHoldRecognizer(_target, _point, voidStationaryHold[_fingerID.ToString()], _fingerID);

	}

	private bool canRegisterShake = true;
	void RegisterShake(float _shakeMagnintudeX, float _shakeMagnintudeY)
	{
		if (canRegisterShake)
		{
			OnShakeRecognizer(_shakeMagnintudeX, _shakeMagnintudeY);
			canRegisterShake = false;
		}
	}

	// OnTouchDown Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3>> onTouchDownSubscribers;
	
	public void SubscribeToOnTouchDown(System.Action<GameObject, Vector3> subscriber) 
	{
		if (onTouchDownSubscribers == null) 
		{
			onTouchDownSubscribers = new List<System.Action<GameObject, Vector3>>();
		}
		
		onTouchDownSubscribers.Add(subscriber);
	}
	
	public void UnsubscribeToOnTouchDown(System.Action<GameObject, Vector3> subscriber) 
	{
		if (onTouchDownSubscribers.Contains(subscriber)) 
		{
			onTouchDownSubscribers.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromOnTouchDown() {
		onTouchDownSubscribers.Clear();
	}
	
	void OnTouchDownRecognizer(GameObject _target, Vector3 _point)
	{
		if (onTouchDownSubscribers != null)
		{
			foreach (System.Action<GameObject, Vector3> eachSubscriber in onTouchDownSubscribers) {
				eachSubscriber(_target, _point);
			}
		}
	}
	#endregion
	
	// OnTouchHold Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3, Vector3, int>> onTouchHoldSubscribers;
	
	public void SubscribeToOnTouchHold(System.Action<GameObject, Vector3, Vector3, int> subscriber) 
	{
		if (onTouchHoldSubscribers == null) 
		{
			onTouchHoldSubscribers = new List<System.Action<GameObject, Vector3, Vector3, int>>();
		}
		
		onTouchHoldSubscribers.Add(subscriber);
	}
	
	public void UnsubscribeToOnTouchHold(System.Action<GameObject, Vector3, Vector3, int> subscriber) 
	{
		if (onTouchHoldSubscribers.Contains(subscriber)) 
		{
			onTouchHoldSubscribers.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromOnTouchHold() 
	{
		onTouchHoldSubscribers.Clear();
	}
	
	void OnTouchHoldRecognizer(GameObject _target, Vector3 _startingPoint, Vector3 _point, int _fingerID)
	{
		if (onTouchHoldSubscribers != null)
		{
			foreach (System.Action<GameObject, Vector3, Vector3, int> eachSubscriber in onTouchHoldSubscribers) {
					eachSubscriber(_target, _startingPoint, _point, _fingerID);
			}
		}
	}
	#endregion
	
	// OnTouchUp Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3, Vector3, int>> onTouchUpSubscribers;
	
	public void SubscribeToOnTouchUp(System.Action<GameObject, Vector3, Vector3, int> subscriber) {
		if (onTouchUpSubscribers == null) {
			onTouchUpSubscribers = new List<System.Action<GameObject, Vector3, Vector3, int>>();
		}
		
		onTouchUpSubscribers.Add(subscriber);
	}
	
	public void UnsubscribeToOnTouchUp(System.Action<GameObject, Vector3, Vector3, int> subscriber) {
		if (onTouchUpSubscribers.Contains(subscriber)) {
			//onTouchUpSubscribers.Remove(subscriber);
			removalList.Add(subscriber);
		}
	}
	
	public void unsubscribeAllFromOnTouchUp() {
		onTouchUpSubscribers.Clear();
	}
	
	List<System.Action<GameObject, Vector3, Vector3, int>> removalList = new List<System.Action<GameObject, Vector3, Vector3, int>>();
	void OnTouchUpRecognizer(GameObject _target, Vector3 _startingInputPos, Vector3 _point, int _fingerID)
	{
		if (removalList != null)
		{
			foreach (System.Action<GameObject, Vector3, Vector3, int> rl in removalList)
			{
				onTouchUpSubscribers.Remove(rl);
			}
		}
		if (onTouchUpSubscribers != null)
		{
			List<System.Action<GameObject, Vector3, Vector3, int>> tempList = new List<System.Action<GameObject, Vector3, Vector3, int>>(onTouchUpSubscribers);
		
			foreach (System.Action<GameObject, Vector3, Vector3, int> eachSubscriber in tempList) {
				eachSubscriber(_target, _startingInputPos, _point, _fingerID);
			}
		}

		activeObject[_fingerID.ToString()] = null;
	}
	#endregion

	// Stationary Hold Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3, bool, int>> stationaryHoldSubscribers;

	public void SubscribeToStationaryHold(System.Action<GameObject, Vector3, bool, int> subscriber) {
		if (stationaryHoldSubscribers == null) {
			stationaryHoldSubscribers = new List<System.Action<GameObject, Vector3, bool, int>>();
		}
		
		stationaryHoldSubscribers.Add(subscriber);
	}
	
	public void UnsubscribeToStationaryHold(System.Action<GameObject, Vector3, bool, int> subscriber) {
		if (stationaryHoldSubscribers.Contains(subscriber)) {
			stationaryHoldSubscribers.Remove(subscriber);
		}
	}
	
	public void UnsubscribeAllFromStationaryHold() {
		stationaryHoldSubscribers.Clear();
	}
	
	void OnStationaryHoldRecognizer(GameObject _target, Vector3 _point, bool _voidLongHold, int _fingerID)
	{
		
		if (stationaryHoldSubscribers != null)
		{
			foreach (System.Action<GameObject, Vector3, bool, int> eachSubscriber in stationaryHoldSubscribers) {
				eachSubscriber(_target, _point, _voidLongHold, _fingerID);
			}
		}
	}
	#endregion
	
	// Tap Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3>> tapSubscribers;
	
	public void SubscribeToTap(System.Action<GameObject, Vector3> subscriber) {
		if (tapSubscribers == null) {
			tapSubscribers = new List<System.Action<GameObject, Vector3>>();
		}
		
		tapSubscribers.Add(subscriber);
	}
	
	public void UnsubscribeToTap(System.Action<GameObject, Vector3> subscriber) {
		if (tapSubscribers.Contains(subscriber)) {
			tapSubscribers.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromTap() {
		tapSubscribers.Clear();
	}
	
	void OnTapRecognizer(GameObject _target, Vector3 _point)
	{

		if (tapSubscribers != null)
		{
			foreach (System.Action<GameObject, Vector3> eachSubscriber in tapSubscribers) {
				eachSubscriber(_target, _point);
			}
		}
	}
	#endregion
	
	// Double Tap Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3>> doubleTapSubscribers;
	
	public void SubscribeToDoubleTap(System.Action<GameObject, Vector3> subscriber) {
		if (doubleTapSubscribers == null) {
			doubleTapSubscribers = new List<System.Action<GameObject, Vector3>>();
		}
		
		doubleTapSubscribers.Add(subscriber);
	}
	
	public void UnsubscribeToDoubleTap(System.Action<GameObject, Vector3> subscriber) {
		if (doubleTapSubscribers.Contains(subscriber)) {
			doubleTapSubscribers.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromDoubleTap() {
		doubleTapSubscribers.Clear();
	}
	
	void OnDoubleTapRecognizer(GameObject _target, Vector3 _point)
	{
		if (doubleTapSubscribers != null)
		{
			foreach (System.Action<GameObject, Vector3> eachSubscriber in doubleTapSubscribers) {
				eachSubscriber(_target, _point);
			}
		}
	}
	#endregion
	
	// Swipe Start On GameObject Subscribers 
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3, Vector3>> swipeStartOnSubscribers;
	
	public void SubscribeToSwipeStartOn(System.Action<GameObject, Vector3, Vector3> subscriber) {
		if (swipeStartOnSubscribers == null) {
			swipeStartOnSubscribers = new List<System.Action<GameObject, Vector3, Vector3>>();
		}
		
		swipeStartOnSubscribers.Add(subscriber);
	}
	
	public void UnsubscribeToSwipeStartOn(System.Action<GameObject, Vector3, Vector3> subscriber) {
		if (swipeStartOnSubscribers.Contains(subscriber)) {
			swipeStartOnSubscribers.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromSwipeStartOn() {
		swipeStartOnSubscribers.Clear();
	}
		
	void OnSwipeStartOnRecognizer(GameObject _target, Vector3 _inputEndingPoint, Vector3 _inputStartingPoint)
	{	
		if (swipeStartOnSubscribers != null)
		{
			foreach (System.Action<GameObject, Vector3, Vector3> eachSubscriber in swipeStartOnSubscribers) {
				eachSubscriber(_target, _inputEndingPoint, _inputStartingPoint);
			}
		}
	}
	
	#endregion
	
	// Swipe End On GameObject Subscribers 
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3, Vector3>> swipeEndOnSubscribers;
	
	public void SubscribeToSwipeEndOn(System.Action<GameObject, Vector3, Vector3> subscriber) {
		if (swipeEndOnSubscribers == null) {
			swipeEndOnSubscribers = new List<System.Action<GameObject, Vector3, Vector3>>();
		}
		
		swipeEndOnSubscribers.Add(subscriber);
	}
	
	public void UnsubscribeToSwipeEndOn(System.Action<GameObject, Vector3, Vector3> subscriber) {
		if (swipeEndOnSubscribers.Contains(subscriber)) {
			swipeEndOnSubscribers.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromSwipeEndOn() {
		swipeEndOnSubscribers.Clear();
	}
		
	void OnSwipeEndOnRecognizer(GameObject _target, Vector3 _inputEndingPoint, Vector3 _inputStartingPoint)
	{	
		if (swipeEndOnSubscribers != null)
		{
			foreach (System.Action<GameObject, Vector3, Vector3> eachSubscriber in swipeEndOnSubscribers) {
				eachSubscriber(_target, _inputEndingPoint, _inputStartingPoint);
			}
		}
	}
	
	#endregion

	// On Shake Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<float, float>> onShakeSubscribers;
	
	public void SubscribeToShake(System.Action<float, float> subscriber)
	{
		if (onShakeSubscribers == null)
			onShakeSubscribers = new List<System.Action<float, float>>();
		
		onShakeSubscribers.Add(subscriber);
	}

	public void UnsubscribeFromShake(System.Action<float, float> subscriber)
	{
		onShakeSubscribers.Remove(subscriber);
	}
	
	void OnShakeRecognizer(float _shakeMagnituteX, float _shakeMagnituteY)
	{
		if (onShakeSubscribers != null)
		{
			foreach (System.Action<float, float> eachSubscriber in onShakeSubscribers)
			{
				eachSubscriber(_shakeMagnituteX, _shakeMagnituteY);
			}
		}
	}
	#endregion


	public void UnsubscribeFromAllTwoParamInput(System.Action<GameObject, Vector3> subscriber) 
	{
		
		if (onTouchDownSubscribers != null)
			if (onTouchDownSubscribers.Contains(subscriber))
				onTouchDownSubscribers.Remove(subscriber);
		
		if (tapSubscribers != null)
			if (tapSubscribers.Contains(subscriber))
				tapSubscribers.Remove(subscriber);
		
		if (doubleTapSubscribers != null)
			if (doubleTapSubscribers.Contains(subscriber))
				doubleTapSubscribers.Remove(subscriber);
	}
	
	public void UnsubscribeFromAllThreeParamInput(System.Action<GameObject, Vector3, Vector3> subscriber)
	{
		if (swipeStartOnSubscribers != null)
			if (swipeStartOnSubscribers.Contains(subscriber))
				swipeStartOnSubscribers.Remove(subscriber);
		
		if (swipeEndOnSubscribers != null)
			if (swipeEndOnSubscribers.Contains(subscriber))
				swipeEndOnSubscribers.Remove(subscriber);

		/*
		if (onTouchHoldSubscribers != null)
			if (onTouchHoldSubscribers.Contains(subscriber))
				onTouchHoldSubscribers.Remove(subscriber);	

		if (onTouchUpSubscribers != null)
			if (onTouchUpSubscribers.Contains(subscriber))
				UnsubscribeToOnTouchUp(subscriber);
		//onTouchUpSubscribers.Remove(subscriber);
		*/
		/*
		if (longHoldSubscribers != null)
			if (longHoldSubscribers.Contains(subscriber))
				UnsubscribeToLongHold(subscriber);
		//onTouchUpSubscribers.Remove(subscriber);
		*/
	}


	
	
	////////////////////////// UI CAM
	
	// OnTouchDown Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3>> onTouchDownSubscribers_UI;
	
	public void SubscribeToOnTouchDown_UI(System.Action<GameObject, Vector3> subscriber) 
	{
		if (onTouchDownSubscribers_UI == null) 
		{	
			onTouchDownSubscribers_UI = new List<System.Action<GameObject, Vector3>>();
		}
		
		onTouchDownSubscribers_UI.Add(subscriber);
	}
	
	public void UnsubscribeToOnTouchDown_UI(System.Action<GameObject, Vector3> subscriber) 
	{
		if (onTouchDownSubscribers_UI.Contains(subscriber)) 
		{
			onTouchDownSubscribers_UI.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromOnTouchDown_UI() {
		onTouchDownSubscribers_UI.Clear();
	}
	
	void OnTouchDownRecognizer_UI(GameObject _target, Vector3 _point)
	{
		if (onTouchDownSubscribers_UI != null)
		{
			foreach (System.Action<GameObject, Vector3> eachSubscriber in onTouchDownSubscribers_UI) {
				eachSubscriber(_target, _point);
			}
		}
	}
	#endregion
	
	// OnTouchHold Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3>> onTouchHoldSubscribers_UI;
	
	public void SubscribeToOnTouchHold_UI(System.Action<GameObject, Vector3> subscriber) 
	{
		if (onTouchHoldSubscribers_UI == null) 
		{
			onTouchHoldSubscribers_UI = new List<System.Action<GameObject, Vector3>>();
		}
		
		onTouchHoldSubscribers_UI.Add(subscriber);
	}
	
	public void UnsubscribeToOnTouchHold_UI(System.Action<GameObject, Vector3> subscriber) 
	{
		if (onTouchHoldSubscribers_UI.Contains(subscriber)) 
		{
			onTouchHoldSubscribers_UI.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromOnTouchHold_UI() 
	{
		onTouchHoldSubscribers_UI.Clear();
	}
	
	void OnTouchHoldRecognizer_UI(GameObject _target, Vector3 _point)
	{
		if (onTouchHoldSubscribers_UI != null)
		{
			foreach (System.Action<GameObject, Vector3> eachSubscriber in onTouchHoldSubscribers_UI) {
				eachSubscriber(_target, _point);
			}
		}
	}
	#endregion
	
	// OnTouchUp Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3>> onTouchUpSubscribers_UI;
	
	public void SubscribeToOnTouchUp_UI(System.Action<GameObject, Vector3> subscriber) {
		if (onTouchUpSubscribers_UI == null) {
			onTouchUpSubscribers_UI = new List<System.Action<GameObject, Vector3>>();
		}
		
		onTouchUpSubscribers_UI.Add(subscriber);
	}
	
	public void UnsubscribeToOnTouchUp_UI(System.Action<GameObject, Vector3> subscriber) {
		if (onTouchUpSubscribers_UI.Contains(subscriber)) {
			//onTouchUpSubscribers.Remove(subscriber);
			removalList_UI.Add(subscriber);
		}
	}
	
	public void unsubscribeAllFromOnTouchUp_UI() {
		onTouchUpSubscribers_UI.Clear();
	}
	
	List<System.Action<GameObject, Vector3>> removalList_UI = new List<System.Action<GameObject, Vector3>>();
	void OnTouchUpRecognizer_UI(GameObject _target, Vector3 _point)
	{
		if (removalList != null)
		{
			foreach (System.Action<GameObject, Vector3> rl in removalList_UI)
			{
				print ("REMOVE");
				onTouchUpSubscribers_UI.Remove(rl);
			}
		}
		if (onTouchUpSubscribers_UI != null)
		{
			foreach (System.Action<GameObject, Vector3> eachSubscriber in onTouchUpSubscribers_UI) {
				eachSubscriber(_target, _point);
			}
		}
	}
	#endregion
	
	// Tap Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3>> tapSubscribers_UI;
	
	public void SubscribeToTap_UI(System.Action<GameObject, Vector3> subscriber) {
		if (tapSubscribers_UI == null) {
			tapSubscribers_UI = new List<System.Action<GameObject, Vector3>>();
		}
		
		tapSubscribers_UI.Add(subscriber);
	}
	
	public void UnsubscribeToTap_UI(System.Action<GameObject, Vector3> subscriber) {
		if (tapSubscribers_UI.Contains(subscriber)) {
			tapSubscribers_UI.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromTap_UI() {
		tapSubscribers_UI.Clear();
	}
	
	void OnTapRecognizer_UI(GameObject _target, Vector3 _point)
	{
		if (tapSubscribers_UI != null)
		{
			foreach (System.Action<GameObject, Vector3> eachSubscriber in tapSubscribers_UI) {
				if (_target == eachSubscriber.Target)
					eachSubscriber(_target, _point);
			}
		}
	}
	#endregion
	
	// Tap Subscribers
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3>> doubleTapSubscribers_UI;
	
	public void SubscribeToDoubleTap_UI(System.Action<GameObject, Vector3> subscriber) {
		if (doubleTapSubscribers_UI == null) {
			doubleTapSubscribers_UI = new List<System.Action<GameObject, Vector3>>();
		}
		
		doubleTapSubscribers_UI.Add(subscriber);
	}
	
	public void UnsubscribeToDoubleTap_UI(System.Action<GameObject, Vector3> subscriber) {
		if (doubleTapSubscribers_UI.Contains(subscriber)) {
			doubleTapSubscribers_UI.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromDoubleTap_UI() {
		doubleTapSubscribers_UI.Clear();
	}
	
	void OnDoubleTapRecognizer_UI(GameObject _target, Vector3 _point)
	{
		if (doubleTapSubscribers_UI != null)
		{
			foreach (System.Action<GameObject, Vector3> eachSubscriber in doubleTapSubscribers_UI) {
				if (_target == eachSubscriber.Target)
					eachSubscriber(_target, _point);
			}
		}
	}
	#endregion
	
	// Swipe Subscribers 
	#region
	[System.NonSerializedAttribute] public List<System.Action<GameObject, Vector3>> swipeSubscribers_UI;
	
	public void SubscribeToSwipe_UI(System.Action<GameObject, Vector3> subscriber) {
		if (swipeSubscribers_UI == null) {
			swipeSubscribers_UI = new List<System.Action<GameObject, Vector3>>();
		}
		
		swipeSubscribers_UI.Add(subscriber);
	}
	
	public void UnsubscribeToSwipe_UI(System.Action<GameObject, Vector3> subscriber) {
		if (swipeSubscribers_UI.Contains(subscriber)) {
			swipeSubscribers_UI.Remove(subscriber);
		}
	}
	
	public void unsubscribeAllFromSwipe_UI() {
		swipeSubscribers_UI.Clear();
	}
		
	void OnSwipeRecognizer_UI(GameObject _target, Vector3 _point)
	{		
		foreach (System.Action<GameObject, Vector3> eachSubscriber in swipeSubscribers_UI) {
			if (_target == eachSubscriber.Target)
				eachSubscriber(_target, _point);
		}
	}
	
	#endregion
	
	public void UnsubscribeFromAllInput_UI(System.Action<GameObject, Vector3> subscriber) {
		/*
		if (onTouchDownSubscribers_UI != null)
			if (onTouchDownSubscribers_UI.Contains(subscriber))
				onTouchDownSubscribers_UI.Remove(subscriber);
		
		if (onTouchHoldSubscribers_UI != null)
			if (onTouchHoldSubscribers_UI.Contains(subscriber))
				onTouchHoldSubscribers_UI.Remove(subscriber);	
		
		if (onTouchUpSubscribers_UI != null)
			if (onTouchUpSubscribers_UI.Contains(subscriber))
				UnsubscribeToOnTouchUp_UI(subscriber);
				//onTouchUpSubscribers.Remove(subscriber);
		
		if (tapSubscribers_UI != null)
			if (tapSubscribers_UI.Contains(subscriber))
				tapSubscribers_UI.Remove(subscriber);
		
		if (doubleTapSubscribers != null)
			if (doubleTapSubscribers_UI.Contains(subscriber))
				doubleTapSubscribers_UI.Remove(subscriber);
		
		if (swipeSubscribers_UI != null)
			if (swipeSubscribers_UI.Contains(subscriber))
				swipeSubscribers_UI.Remove(subscriber);
		*/
	}
}
