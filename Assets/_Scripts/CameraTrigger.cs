﻿using UnityEngine;
using System.Collections;

public class CameraTrigger : MonoBehaviour 
{
	public Transform mCameraTarget;





	// Use this for initialization
	void Start () 
	{
		mCameraTarget = transform.parent.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnTriggerEnter(Collider c)
	{
		if(c.gameObject.tag == "Player")
		{


			iTween.MoveTo(Camera.main.transform.gameObject, iTween.Hash("x", mCameraTarget.transform.position.x, "y", mCameraTarget.transform.position.y, "easeType", "easeOutSine"));
			Debug.Log("Player entered");
		}
	}

}
