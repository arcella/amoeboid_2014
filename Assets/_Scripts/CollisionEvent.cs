﻿using UnityEngine;
using System.Collections;

public class CollisionEvent : MonoBehaviour {

	public CollisionTargets[] colTargets;

	void OnCollisionEnter2D(Collision2D coll)
	{
		foreach (CollisionTargets ct in colTargets)
		{
			if (coll.gameObject.name == ct.collisionPerpName || ct.collisionPerpName == "" || ct.collisionPerpTag == coll.gameObject.tag)
			{
				if (ct.param == "")
					ct.messageReceiver.SendMessage(ct.message);
				else
					ct.messageReceiver.SendMessage(ct.message, ct.param);
			}
		}
	}
}

[System.Serializable]
public class CollisionTargets
{
	public GameObject messageReceiver;
	public string collisionPerpName = "";
	public string collisionPerpTag = "";
	public string message;
	public string param = "";
}
