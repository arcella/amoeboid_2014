﻿using UnityEngine;
using System.Collections;

public class LevelEnd : MonoBehaviour 
{
	public Color mColor1;
	public Color mColor2;

	public float mChangeSpeed = 1.0f;

	public AudioClip winSound;

	public MobileClient mobileClient;


	// Use this for initialization
	void Start () {
		mobileClient = GameObject.Find("MobileClient").GetComponent<MobileClient>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//this.renderer.material.color = Color.Lerp(mColor1, mColor2, Time.time);
		iTween.ColorTo(gameObject, iTween.Hash("color", mColor2, "loopType", "pingPong", "speed", mChangeSpeed));
	}

	void OnTriggerEnter(Collider c)
	{
		if(c.gameObject.tag == "Player")
		{
			audio.PlayOneShot(winSound);
			mobileClient.isFirstToggled = true;
			mobileClient.isFireStarted = true;

		}
	}
	
}
