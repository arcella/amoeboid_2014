﻿using UnityEngine;
using System.Collections;

public class ConveyorBelt : MonoBehaviour {

	public float scrollSpeed = 0.5F;

	void Update() {
		float offset = Time.time * scrollSpeed;
		renderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
		renderer.material.SetTextureOffset("_BumpMap", new Vector2(offset, 0));
	}

	void OnCollisionStay(Collision c)
	{
		if(c.gameObject.tag == "Player")
		{
			c.rigidbody.AddForce (new Vector3(scrollSpeed, 0, 0));
			Debug.Log("CONVEYOR");
		}
	}
}
