﻿using UnityEngine;
using System.Collections;

public class MobileClient : MonoBehaviour {

	public Multipass multiPass;

	public string targetDevice = "";

	public bool 	isFirstToggled = false;


	#region :::::::::::::::: STATES
	public  bool isFireStarted = false;
	public  bool isGravityInverted = false;

	#endregion

	void Update()
	{
		if(isFireStarted == true && (isFirstToggled == true) )
		{
			multiPass.SendRPC("StartFire", targetDevice);
		}

		if(Input.deviceOrientation == DeviceOrientation.FaceDown && (isFirstToggled == true) )
		{
			isGravityInverted = true;
		}
		else
		{
			isGravityInverted = false;
		}


		if( (isGravityInverted == true) && (isFirstToggled == true) )
		{
			multiPass.SendRPC("InvertGravity", targetDevice);
			isFirstToggled = false;
		}


	}


	void OnGUI(){
		if(targetDevice == ""){
			string[] ids = multiPass.GetComputerIds();
			foreach(string str in ids){
				if(GUILayout.Button(str, GUILayout.Width(300), GUILayout.Height(100))){
					targetDevice = str;
				}
			}
		}
		else{
			if(GUILayout.Button("Detatch", GUILayout.Width(100), GUILayout.Height(100)))
				targetDevice = "";

			if(GUILayout.Button("Fire", GUILayout.Width(100), GUILayout.Height(100)))
				multiPass.SendRPC("StartFire", targetDevice);
			
			if(GUILayout.Button("Gravity", GUILayout.Width(100), GUILayout.Height(100)))
				multiPass.SendRPC("InvertGravity", targetDevice);
			
			if(GUILayout.Button("Lights", GUILayout.Width(100), GUILayout.Height(100)))
				multiPass.SendRPC("TurnOnLights", targetDevice);

			if(GUILayout.Button("Debug", GUILayout.Width(100), GUILayout.Height(100)))
				multiPass.SendDebug("Hello World!", targetDevice);

			if(GUILayout.Button("TOGGLE", GUILayout.Width(100), GUILayout.Height(100)))
				isFirstToggled = !isFirstToggled;
		}


		GUI.Label(new Rect(10,110,200,100),"Device Orientation " + Input.deviceOrientation.ToString());

		if(isGravityInverted)
		{
			GUI.Label(new Rect(100,410,200,100),"INVERTED");
		}

	}
}
