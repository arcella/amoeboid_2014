﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Linq;
using System.IO;

public class Multipass : MonoBehaviour {
	//announce constants
	const string COMP_IDENT = "IMACOMPUTER";
	const string MOBILE_IDENT = "GIJOOOOEEEE";

	//game constants
	const byte RPC_HEADER = 0xFE;
	const byte DEBUG_HEADER = 0xFD;

	UdpClient announceClient;
	UdpClient gameClient;
	IPAddress localIP;
	IPEndPoint multiEndPoint;
	bool isMobile;


	Dictionary<string, IPEndPoint> computerEndPoints = new Dictionary<string, IPEndPoint>();
	Dictionary<string, IPEndPoint> mobileEndPoints = new Dictionary<string, IPEndPoint>();
	Dictionary<string, IPEndPoint> allEndPoints = new Dictionary<string, IPEndPoint>();

	public int announcePort = 2020;
	public int gamePort = 2040;

	public bool debugGui = true;

	public GameObject RPCReceiver;

	void Start () {
		if(Application.platform == RuntimePlatform.Android)
			isMobile = true;

		announceClient = new UdpClient();

		IPAddress multiAddress = IPAddress.Parse("239.0.0.100");

		announceClient.JoinMulticastGroup(multiAddress);
		announceClient.MulticastLoopback = false;
		multiEndPoint = new IPEndPoint(multiAddress, announcePort);

		localIP = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);

		announceClient.Client.Bind ( new IPEndPoint(IPAddress.Any, announcePort));

		if(localIP != null){
			StartCoroutine(AnnounceLoop());

			gameClient = new UdpClient();
			gameClient.Client.Bind( new IPEndPoint(IPAddress.Any, gamePort));

		}
		else{
			Debug.LogError("Local IP is null!");
		}


	}

	IEnumerator AnnounceLoop(){
		while(true){
			AnnounceSelf();
			yield return new WaitForSeconds(5.0f);
		}
	}

	public string[] GetAllDeviceIds(){
		return allEndPoints.Keys.ToArray();
	}

	public string[] GetMobileIds(){
		return mobileEndPoints.Keys.ToArray();
	}

	public string[] GetComputerIds(){
		return computerEndPoints.Keys.ToArray();
	}

	void OnGUI(){

		if(debugGui){
			GUILayout.BeginHorizontal();
			{
				if(GUILayout.Button("ANNOUNCE", GUILayout.Width(100), GUILayout.Height(100)))
					AnnounceSelf();
				if(GUILayout.Button("CLEAR", GUILayout.Width(100), GUILayout.Height(100))){
					computerEndPoints.Clear();
					mobileEndPoints.Clear();
					allEndPoints.Clear();
				}

			}
			GUILayout.EndHorizontal();

			GUILayout.Label("Computers:");
			foreach(IPEndPoint ep in computerEndPoints.Values){
				GUILayout.Label(ep.ToString());
			}

			GUILayout.Space(48);

			GUILayout.Label("Mobiles:");
			foreach(IPEndPoint ep in mobileEndPoints.Values){
				GUILayout.Label(ep.ToString());
			}
		}
	}

	void AnnounceSelf(){
		AnnounceSelf(multiEndPoint);
	}

	void AnnounceSelf(IPEndPoint ep){
		byte[] buffer = null;
		if(isMobile)
			buffer = Encoding.ASCII.GetBytes( string.Format("{0} {1} {2} {3} {4}", MOBILE_IDENT, SystemInfo.deviceUniqueIdentifier.Replace(" ", "_"), localIP.ToString(), gamePort, SystemInfo.deviceName));
		else
			buffer = Encoding.ASCII.GetBytes( string.Format("{0} {1} {2} {3} {4}", COMP_IDENT, SystemInfo.deviceUniqueIdentifier.Replace(" ", "_"), localIP.ToString(), gamePort, SystemInfo.deviceName));
		
		announceClient.Send( buffer, buffer.Length, ep );
	}
	

	void Update () {
		if(announceClient.Available > 0){
			ProcessAnnouncePacket();
		}

		if(gameClient.Available > 0){
			ProcessGamePacket();
		}
	}


	void ProcessAnnouncePacket(){
		IPEndPoint sender = null;
		byte[] data = announceClient.Receive(ref sender);
		string msg = Encoding.ASCII.GetString(data);

		string[] chunks = msg.Split(' ');
		switch(chunks[0]){
		case COMP_IDENT:
			if(!computerEndPoints.ContainsKey(chunks[1])){
				computerEndPoints.Add(chunks[1], new IPEndPoint(IPAddress.Parse(chunks[2]), int.Parse(chunks[3])));
				allEndPoints.Add(chunks[1], new IPEndPoint(IPAddress.Parse(chunks[2]), int.Parse(chunks[3])));
			}

			break;
		case MOBILE_IDENT:
			if(!mobileEndPoints.ContainsKey(chunks[1])){
				mobileEndPoints.Add(chunks[1], new IPEndPoint(IPAddress.Parse(chunks[2]), int.Parse(chunks[3])));
				allEndPoints.Add(chunks[1], new IPEndPoint(IPAddress.Parse(chunks[2]), int.Parse(chunks[3])));
			}

			
			break;

		default:
			Debug.LogWarning("Announce: Unknown packet type!");
			break;
		}
	}

	void ProcessGamePacket(){
		IPEndPoint sender = null;
		byte[] data = gameClient.Receive(ref sender);

		BinaryReader rd = new BinaryReader(new MemoryStream(data));
		byte header = rd.ReadByte();

		switch(header){
		case RPC_HEADER:
			ProcessRPC(rd);
			break;
		case DEBUG_HEADER:
			ProcessDebug(rd);
			break;
		default:
			Debug.LogWarning("Game: Unknown packet type!");
			break;
		}

	}

	void ProcessRPC(BinaryReader rd){
		string functionName = rd.ReadString();

		//TODO:  Handle at least 1 parameter

		if(RPCReceiver != null)
			RPCReceiver.SendMessage(functionName);
		else
			Debug.LogWarning("No RPC Receiver!");
	}

	void ProcessDebug(BinaryReader rd){
		Debug.Log ("<color=green>" + rd.ReadString() + "</color>");
	}

	public void SendRPC(string functionName, string deviceId){
		IPEndPoint ep = null;

		if(allEndPoints.ContainsKey(deviceId))
			ep = allEndPoints[deviceId];

		if(ep == null){
			Debug.LogError("Send Error: Device ID Unknown!");
			return;
		}

		MemoryStream stream = new MemoryStream();
		BinaryWriter wr = new BinaryWriter(stream);
		wr.Write (RPC_HEADER);
		wr.Write (functionName);
		//TODO:  write parameter count

		byte[] data = stream.ToArray();
		gameClient.Send(data, data.Length, ep);
	}

	public void SendDebug(string message, string deviceId){
		IPEndPoint ep = null;
		
		if(allEndPoints.ContainsKey(deviceId))
			ep = allEndPoints[deviceId];
		
		if(ep == null){
			Debug.LogError("Send Error: Device ID Unknown!");
			return;
		}
		
		MemoryStream stream = new MemoryStream();
		BinaryWriter wr = new BinaryWriter(stream);
		wr.Write (DEBUG_HEADER);
		wr.Write (message);
		
		byte[] data = stream.ToArray();
		gameClient.Send(data, data.Length, ep);
	}

}
