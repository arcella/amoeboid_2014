﻿using UnityEngine;
using System.Collections;

public class enemy_Paramecium : MonoBehaviour 
{
	public Transform 	mMoveTarget;
	public float		mSpeed = 1.75f;

	public Color mColor1;
	public Color mColor2;
	
	public float mChangeSpeed = 1.0f;


	// Use this for initialization
	void Start () 
	{
		mMoveTarget = transform.parent.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		iTween.MoveTo(gameObject, iTween.Hash(	"x", mMoveTarget.transform.position.x,
												"y",mMoveTarget.transform.position.y, 
												"easeType", "easeInOutCubic", 
												"loopType", "pingPong", 
												"speed", mSpeed));
		
		iTween.RotateBy(gameObject, iTween.Hash("x", .25, 
												"easeType", "easeInOutBack", 
												"loopType", "pingPong", 
												"delay", .4));
		
		
		iTween.ColorTo(gameObject, iTween.Hash("color", mColor2, "loopType", "pingPong", "speed", mChangeSpeed));
	}

}
