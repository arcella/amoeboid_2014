﻿using UnityEngine;
using System.Collections;


public class KnockBackEnemy : MonoBehaviour 
{
	//private AmoebaController 	amoebaController;
	private Controller			controller;
	
	public int		mDamageAmount;
	public float	mExplosionForce = 100;
	public Vector3 	mExplosionVect;
	public float	mExplosionRad;
	
	public AudioClip	mDamageAudio;
	
	public GameObject	p_effect;
	
	void OnCollisionEnter (Collision other)
	{
		if(other.gameObject.name != "InputCollider"){
		
			if(other.gameObject.tag == "Player")
			{
				//controller = other.gameObject.GetComponent<Controller>();
				controller = other.gameObject.GetComponentInChildren<Controller>();
				controller.DamageHealth(mDamageAmount);
	
				//amoebaController = other.gameObject.GetComponent<AmoebaController>();
				//amoebaController.DamageHealth(mDamageAmount);
	
				if (controller.mHealth > 0)
				{
					other.rigidbody.AddForce(this.rigidbody.velocity * mExplosionForce);
				}
	
	
				Instantiate(p_effect, transform.position, Quaternion.identity);
				audio.PlayOneShot(mDamageAudio, .5f);
	
				
			}
		}
	}
}
