﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{

			GameObject.Find("_CheckPointManager").GetComponent<CheckPointManager>().mSpawnPosition = transform.position;
			GameObject.Find("_CheckPointManager").GetComponent<CheckPointManager>().mCheckPointIndicator.transform.position = transform.position + new Vector3(0,2.5f,0);
			GameObject.Find("_CheckPointManager").GetComponent<CheckPointManager>().UpdateCheckPoint();
		}
	}

}
