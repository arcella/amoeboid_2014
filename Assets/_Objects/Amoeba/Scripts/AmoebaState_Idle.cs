﻿using UnityEngine;
using System.Collections;

public class AmoebaState_Idle : AmoebaBase
{
	private AmoebaController amoebaController;

	void OnEnable()
	{
		
		//amoebaController = GetComponent<AmoebaController>();
		//amoebaController.isEnteringState = true;


		mAnimationMesh.SetActive(true);
		mStretchMesh.SetActive(false);
		anim = mAnimationMesh.GetComponent<Animator>();
		anim.SetInteger("AnimationIndex", 0);
		
		
		Debug.Log ("IDLE");
	}
	
	
	void OnDisable()
	{
		
	}
	
		
	void Start ()
	{

	}

	void Update ()
	{
		
	}
	

	
}
