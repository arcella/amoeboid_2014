﻿using UnityEngine;
using System.Collections;

public class CheckPointIndicator : MonoBehaviour {

	public Vector3 rotationVector;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//iTween.RotateBy(gameObject, new Vector3(.5f,0,5));
		transform.Rotate( rotationVector);
	}
}
