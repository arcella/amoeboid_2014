﻿using UnityEngine;
using System.Collections;

public class AmoebaController : AmoebaBase
{
	public enum mAmoebaStates
	{
		Idle,
		Alert,
		StretchFling,
		Landing,
		Squishing,
		Win,
		Dead
	}
	
	
	public mAmoebaStates currentState;
	public bool isChangingState;
	
	#region ::::::::::::::::::::::: Member Variables
	// movement
	public 	bool 		isGrounded;
	public 	bool		isWallSticking;
	public	bool		isAirSwipe;
	public	float		mAirSwipeRechargeTimer = 1f;

	public 	float 		mFlingForce = 10;
	public 	float 		mMaxStretchDist = 2.5f;
	private Rigidbody	mRigidbody;
	
	private Vector3 	mCurrentPosition;
	private Vector3 	mTargetPosition;

	// UI
	public	GameObject	mAirSwipeRechargeGraphics;

	// Scripts
	public MonoBehaviour[] scripts;

	// Audio
	public AudioClip	mAirSwipeAudio;
	public AudioClip	mFlingAudio;

	// FX
	public GameObject	mSwipePart;
	

	#endregion
	
	#region ::::::::::::::::::::::: Initialize
	protected override void Initialize ()
	{
		mRigidbody = GetComponent<Rigidbody>();
		playerTransform = this.transform;
		currentState = mAmoebaStates.Idle;
		isChangingState = false;

		
		isDead = false;
		mHealth = 100;
	
	}

	#endregion
	
	#region ::::::::::::::::::::::: UPDATE ::::: STATE MACHINE
	protected override void FSMUpdate ()
	{
//		if(mHealth <= 0)
//		{
//			Debug.Log("YOU ARE DEAD");
//		}
		
		if(isChangingState)
		{
			Debug.Log("CHANGING STATE ****************");
		}

		switch (currentState) {
			
		case mAmoebaStates.Idle:
			if(isChangingState)
			{
				StateScriptActivation(scripts[0]);
			}
			
			if(Input.GetKeyDown(KeyCode.A))
			{
				currentState = mAmoebaStates.Alert;
				isChangingState = true;
			}
			break;
			
		case mAmoebaStates.Alert:
			if(isChangingState)
			{
				StateScriptActivation(scripts[1]);
			}
			
			if(Input.GetKeyDown(KeyCode.B))
			{
				currentState = mAmoebaStates.Idle;
				isChangingState = true;
			}
			break;
			
		case mAmoebaStates.StretchFling:
			StateScriptActivation(scripts[2]);
			break;
			
		case mAmoebaStates.Landing:
			StateScriptActivation(scripts[3]);
			break;
			
		case mAmoebaStates.Squishing:
			StateScriptActivation(scripts[4]);
			break;
			
		case mAmoebaStates.Win:
			StateScriptActivation(scripts[5]);
			break;
			
		case mAmoebaStates.Dead:
			StateScriptActivation(scripts[6]);
			break;
		}
	}
	
	#endregion
	
	#region ::::::::::::::::::::::: FIXED UPDATE :::::
	protected override void FSMFixedUpdate ()
	{
		
	}
	
	#endregion
	
	
	public void StateScriptActivation(MonoBehaviour scriptToActivate)
	{
		foreach(MonoBehaviour script in scripts)
		{
			script.enabled = false;
		}
		
		scriptToActivate.enabled = true;
		isChangingState = false;
	}

	// ::::::::::::: Grounded / Wall check
	void OnCollisionEnter (Collision other)
	{
		if(other.gameObject.tag == "Ground")
		{
			//Debug.Log(other.gameObject.tag);
			isGrounded = true;
			ResetAirSwipe();
			currentState = mAmoebaStates.Idle;

		// GROUND ORIENTATION
		ContactPoint contact = other.contacts[0];
		Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
		transform.rotation = rot;
		
		//return;
		}

		if (other.gameObject.tag == "Wall")
		{
			Debug.Log(other.gameObject.tag + "Wall Sticking test");
			isWallSticking = true;
			ResetAirSwipe();

			ContactPoint contact = other.contacts[0];
			Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
			transform.rotation = rot;
			mRigidbody.drag = 20.0f;
		}
		else
		{

			Quaternion rot = Quaternion.Euler(0,0,0);
		}
	}
	
	void OnCollisionExit (Collision other)
	{
		isGrounded = false;
		isWallSticking = false;
		mRigidbody.drag = .5f;

	}

	public void ResetAirSwipe()
	{
		isAirSwipe = true;
		mAirSwipeRechargeTimer = 1;
	}

	public void AirSwipeGUI()
	{
		mAirSwipeRechargeGraphics.renderer.material.SetFloat("_Cutoff", Mathf.InverseLerp(0, 1, mAirSwipeRechargeTimer)); 
	}



	#region :::::::::::::::::::::::::: GUI ::::::::::
	void OnGUI()
	{
		//GUI.Box(new Rect(10,10,200,70), "current State " + currentState + "\n is Grounded " + isGrounded + "\n is Wall Sticking" + isWallSticking);
		GUI.Box(new Rect(10,100,200,50), " Health " + mHealth);

	}
	#endregion
	
}
