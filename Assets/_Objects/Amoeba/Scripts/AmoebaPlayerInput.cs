using UnityEngine;
using System.Collections;

public class AmoebaPlayerInput : MonoBehaviour
{
	
	// ::::::::::::: FSM
	public enum AmoebaStates
	{
		idle,
		aiming,
		moving,
		squishing
	}
	
	public AmoebaStates currentState = AmoebaStates.idle;
	
	// ::::::::::::: Abilities
	
	public bool IsGrounded;
	
	// FLING
	public float flingForce = 10;
	public Vector3 flingVector;
	
	// Edge Grab
	public GameObject grabRadGeom;
	public GameObject grabTarget;
	public float maxStretchDist = 2.5f;
	public Transform grabDest;
	
	//SQUISH
	

	
	

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{		
		StateMachine();
		Pseudopod ();
	}
	
	// ::::::::::::: FSM
	void StateMachine ()
	{
		if(IsGrounded)
			currentState = AmoebaStates.idle;
		
		switch (currentState) {
		case AmoebaStates.idle:
			//Debug.Log(currentState);
			// amoeba is idle
			grabRadGeom.renderer.enabled = false;
			
			break;
			
		case AmoebaStates.aiming:
			grabRadGeom.renderer.enabled = true;
			break;
			
		case AmoebaStates.moving:
			
			//AmoebaFling(grabTarget);
			
			
			break;
			
		case AmoebaStates.squishing:
			
			break;
		}
	}
	
	
	// :::::::::::::::::::::::::::::: METHODS
	public void AmoebaFling(GameObject target)
	{
			flingVector = (target.transform.position - transform.position) * flingForce;
			rigidbody.AddForce(flingVector, ForceMode.Impulse);
			//Debug.Log("Fling    " + flingVector);
		
		
			target.transform.localPosition = Vector3.zero;
			currentState = AmoebaStates.idle;

	}
	
	void Pseudopod ()
	{
		Vector3 stretchVect = (grabTarget.transform.position - this.transform.position);
		float length = stretchVect.magnitude;
		
		if (length > maxStretchDist) {
			stretchVect.Normalize ();
			grabTarget.transform.localPosition = stretchVect * (maxStretchDist);
		}
	}
	
	void OnSwipe(SwipeGesture gesture) 
	{
		//Debug.Log("Swiped " + gesture.Direction + " with finger " + gesture.Fingers[0] + " (velocity:" + gesture.Velocity + ", distance: " + gesture.Move.magnitude + ")");
	}
	
	
	
	// ::::::::::::: Grounded
	void OnCollisionStay (Collision col)
	{
		IsGrounded = true;
	}
 
	void OnCollisionExit (Collision col)
	{
		IsGrounded = false;
	}
	

	
	
	
}
