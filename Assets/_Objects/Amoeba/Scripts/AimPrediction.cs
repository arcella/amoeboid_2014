﻿using UnityEngine;
using System.Collections;

public class AimPrediction : MonoBehaviour 
{

	public 		float 			mForce = 4.0f;
	public 		int				mSamples = 15;
	public 		float			mSpacing = 0.1f;
	
	public		Vector3			mAimPosition;

	private 	Vector3			mOffset;
	public		Vector3			mHome;
	private		Rigidbody		mRb;
	private		GameObject[]	indicator;
	

	public 		Material		mMaterial;
	
	// Amoeba Controller
	private		AmoebaVisuals	amoebaVisuals;



	// Use this for initialization
	void Start () 
	{
		amoebaVisuals = transform.parent.GetComponent<AmoebaVisuals>();
		mMaterial = this.gameObject.renderer.material;

		//mHome = transform.localPosition; 
		mRb = 	rigidbody;
		indicator = 	new GameObject[mSamples];


		for (int i = 0; i < indicator.Length; i++)
		{
			GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			go.renderer.material = mMaterial;
			go.collider.enabled = false;
			go.transform.localScale = new Vector3(0.2f,0.2f,0.2f);
			indicator[i] = go;
		}

	}

	void ReturnHome()
	{
		transform.position = mHome;
		mRb.velocity = Vector3.zero;
		mRb.isKinematic = true;
	}

	void OnMouseDown()
	{
		
		var v3 = Input.mousePosition;
		v3.z = transform.position.z - Camera.main.transform.position.z;
		v3 = Camera.main.ScreenToWorldPoint(v3);
		mOffset = transform.position - v3;
	}

	void OnMouseDrag()
	{
		var v3 = Input.mousePosition;
		v3.z = transform.position.z - Camera.main.transform.position.z;
		v3 = Camera.main.ScreenToWorldPoint(v3);
		transform.position = v3 + mOffset;
		//DisplayIndicators();
	}

	void OnMouseUp()
	{
		mRb.isKinematic = false;
		mRb.velocity = mForce * ( mAimPosition  - mHome);
		Invoke("ReturnHome", 2.0f);
	}

//	void DisplayIndicators()
//	{
//		indicator[0].transform.position = transform.position;
//		var v3 = transform.position;
//		var y = ( mForce * ( transform.position - mHome ) ).y;
//		var t = 0.0f;
//		v3.y = 0.0f;
//
//		for (int i = 1; i < indicator.Length; i++)
//		{
//			v3 += mForce * ( transform.position - mHome) * mSpacing;
//			t += mSpacing;
//			v3.y = y * t + 0.5f * Physics.gravity.y * t * t + transform.position.y;
//			indicator[i].transform.position = v3;
//		}
//	}
	
	
	public void DisplayIndicators()
	{
//		indicator[0].transform.position = mHome;
//		var v3 = mHome;
//		var y = ( mForce * ( mAimPosition - mHome ) ).y;
//		var t = 0.0f;
//		v3.y = 0.0f;
//
//		for (int i = 1; i < indicator.Length; i++)
//		{
//			v3 += mForce * ( mAimPosition - mHome) * mSpacing;
//			t += mSpacing;
//			v3.y = y * t + 0.5f * Physics.gravity.y * t * t + transform.position.y;
//			indicator[i].transform.position = v3;
//		}
	}

	
	// Update is called once per frame
	void Update () {
	
	}
}
