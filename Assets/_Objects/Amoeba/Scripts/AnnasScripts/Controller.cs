﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {
	
	// STATES
	public enum mAmoebaStates
	{
		Idle,
		Alert,
		Flinging,
		Grounded,
		WallCling,
		Landing,
		Squishing,
		Dead
	}
	public mAmoebaStates currentState;
	
	
	// MOVEMENT
	public float 		forceMag = 100f;
	public bool			isGrounded = false;
	public bool			isWallClinging;
	public Transform	mWallClingPos;
	
	
	// STATS
	public int			mHealth = 100;
	
	// OTHER SCRIPTS
	private	AimPrediction	aimPrediction;
	
	
	// Use this for initialization
	void Start () {
		aimPrediction = transform.GetComponentInChildren<AimPrediction>();
		
		
		//Subscribe the Amoeba to all input functionality
		InputManager.main.SubscribeToOnTouchDown(TouchDown);
		InputManager.main.SubscribeToOnTouchHold(TouchHold);
		InputManager.main.SubscribeToOnTouchUp(TouchUp);
	}
	
	
	#region ::::::::::::::::: TOUCH INPUT MANAGER
	void TouchDown(GameObject _go, Vector3 _point)
	{
		print ("touching");
		// Is the player touching this game object?
		if (_go == this.gameObject)
		{
			print("touching amoeba");
			aimPrediction.mHome = _point;
		}
	}
	
	void TouchHold(GameObject _go, Vector3 _startingPoint, Vector3 _point, int _fingerID)
	{
		// Is the object that the player started touching when they first touched down on the screen equal to this game object?
		if (InputManager.main.activeObject[_fingerID.ToString()] == this.gameObject)
		{
			aimPrediction.mHome = _startingPoint;
			aimPrediction.mAimPosition = _point;
			aimPrediction.DisplayIndicators();
			//print("dragging amoeba");
		}
	}
	
	void TouchUp(GameObject _go, Vector3 _startingPoint, Vector3 _point, int _fingerID)
	{
		// Is the object that the player started touching when they first touched down on the screen equal to this game object?
		if (InputManager.main.activeObject[_fingerID.ToString()] == this.gameObject)
		{
			//print("releasing amoeba");
			
			if(isGrounded || isWallClinging)
			{
				currentState = mAmoebaStates.Flinging;
				transform.parent.gameObject.rigidbody.AddForce((_startingPoint-_point)*-forceMag);
				
				isWallClinging = false;
				isGrounded = false;
			}
		}
	}
	
	#endregion
	
	
	void Update()
	{
		switch (currentState) 
		{
			
		case mAmoebaStates.Idle:
			break;
			
		case mAmoebaStates.Grounded:
			
			transform.parent.gameObject.rigidbody.useGravity = true;
			transform.parent.gameObject.rigidbody.isKinematic = false;
			
			break;
			
		case mAmoebaStates.Flinging:
			
			transform.parent.gameObject.rigidbody.useGravity = true;
			transform.parent.gameObject.rigidbody.isKinematic = false;
			break;
			
			
		case mAmoebaStates.WallCling:
			
			isWallClinging = true;
			
			//transform.parent.gameObject.rigidbody.useGravity = false;
			//transform.parent.gameObject.rigidbody.isKinematic = true;
			//transform.parent.gameObject.transform.position = mWallClingPos.transform.position;
			//transform.position = mWallClingPos.transform.position;

			
			
			break;
			
		case mAmoebaStates.Alert:
			break;

		}
	}
	
	
	#region :::::::::::::: CHARACTER ATTRIBUTE FUNCTIONS
	
	public void DamageHealth(int damageAmt)
	{
		mHealth -= damageAmt;
	}
	
	#endregion
	
	#region :::::::::::::: CHARACTER MOVEMENT FUNCTIONS
	public void PollEnvironment()
	{
		
	}
	
	
	#endregion
	
	
	
}
