﻿using UnityEngine;
using System.Collections;

public class AmoebaVisuals : MonoBehaviour
{
	public Transform 		mAmoeba;
	public float			mCastDist = 1;
	public Controller 		mController;
	
	// ANIMATIONS
	public Transform		mAmoebaStretchMesh;
	public Transform		mAmoebaAnimMesh;
	public Transform		mAmoebaStretchJoint;
	public float 			mStretchDist;
	public float 			mMaxStretchDist = 2.5f;
	
	
	// ::::::::::::::::::::::: START
	void Start ()
	{
		//Subscribe the Amoeba to all input functionality
		InputManager.main.SubscribeToOnTouchDown (TouchDown);
		InputManager.main.SubscribeToOnTouchHold (TouchHold);
		InputManager.main.SubscribeToOnTouchUp (TouchUp);
		
		mController = transform.parent.GetComponent<Controller> ();
		mAmoeba = transform.parent;
	}
	
	// ::::::::::::::::::::::: UPDATE
	void Update ()
	{
		OrientAmoeba ();
		
	}
	
	void TouchDown (GameObject _go, Vector3 _point)
	{
		print ("touching");
		// Is the player touching this game object?
		if (_go == this.gameObject) {
			mAmoebaAnimMesh.gameObject.SetActive (false);
			mAmoebaStretchMesh.gameObject.SetActive (true);
		}
	}
	
	void TouchHold (GameObject _go, Vector3 _startingPoint, Vector3 _point, int _fingerID)
	{
		// Is the object that the player started touching when they first touched down on the screen equal to this game object?
		if (InputManager.main.activeObject [_fingerID.ToString ()] == this.gameObject) {
			
			// TODO MAKE THIS LIMITED STRETCH DISTANCE
			mStretchDist = Vector3.Distance (_startingPoint, _point);
			
			Vector3 stretchVect = (mAmoebaStretchJoint.transform.position - transform.position);
			float length = stretchVect.magnitude;
			
			mAmoebaStretchJoint.transform.position = _point;
			
			if (length > mMaxStretchDist) {
				mAmoebaStretchJoint.transform.position = (mAmoebaStretchJoint.transform.position - transform.position).normalized * mMaxStretchDist + transform.position;
			}
			

		}
	}
	
	void TouchUp (GameObject _go, Vector3 _startingPoint, Vector3 _point, int _fingerID)
	{
		// Is the object that the player started touching when they first touched down on the screen equal to this game object?
		if (InputManager.main.activeObject [_fingerID.ToString ()] == this.gameObject) {
			//print("releasing amoeba");
			mAmoebaAnimMesh.gameObject.SetActive (true);
			mAmoebaStretchMesh.gameObject.SetActive (false);
			
		}
	}
	
	void OrientAmoeba ()
	{
		RaycastHit hit;
		//Ray			ray = transform.
		
		if (Physics.Raycast (transform.position, -Vector3.up, out hit, mCastDist)) {
			//float distanceToGround = hit.distance;
			Debug.DrawLine (this.transform.parent.position, hit.point);
			

			mAmoeba.transform.up -= -(hit.normal);
			
		}
	}
}
