﻿using UnityEngine;
using System.Collections;

public class AmoebaCollisionChecker : MonoBehaviour 
{
	
	public Controller 	controller;

	

	// Use this for initialization
	void Start () 
	{
		controller = transform.gameObject.GetComponentInChildren<Controller>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	#region :::::::::::::: CHARACTER COLLISIONS
	
	void OnCollisionEnter ( Collision other )
	{
		if(other.gameObject.tag  == "Ground")
		{
			controller.currentState = Controller.mAmoebaStates.Grounded;
			Debug.Log("GROUND");
			controller.isGrounded = true;
		}
		else 
		{
			controller.isGrounded = false;
		}
		
		if (other.gameObject.tag == "Wall")
		{
			Debug.Log(other.gameObject.tag + "Wall Sticking test");
			
			// set state to wall cling
			controller.currentState = Controller.mAmoebaStates.WallCling;

			ContactPoint contact = other.contacts[0];
			
			//Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
			//transform.rotation = rot;
			
			// set position of wall cling
			controller.mWallClingPos.transform.position = contact.point;
			controller.mWallClingPos.parent = other.transform;

			
			
			
			
			//this.rigidbody.drag = 20.0f;
		}
		

	}
	
	#endregion
	
}
