﻿using UnityEngine;
using System.Collections;

public class HingeController : MonoBehaviour {

	// Use this for initialization
	void Start () {

		//Subscribe the Amoeba to all input functionality
		InputManager.main.SubscribeToOnTouchDown(TouchDown);
		InputManager.main.SubscribeToOnTouchHold(TouchHold);
		InputManager.main.SubscribeToOnTouchUp(TouchUp);
	}

	void TouchDown(GameObject _go, Vector3 _point)
	{
		print ("touching");
		// Is the player touching this game object?
		if (_go == this.gameObject)
		{
			print("touching amoeba");
		}
	}
	
	void TouchHold(GameObject _go, Vector3 _startingPoint, Vector3 _point, int _fingerID)
	{
		// Is the object that the player started touching when they first touched down on the screen equal to this game object?
		if (InputManager.main.activeObject[_fingerID.ToString()] == this.gameObject)
		{
			print("dragging amoeba");
			gameObject.transform.position = _point;
		}
	}
	
	void TouchUp(GameObject _go, Vector3 _startingPoint, Vector3 _point, int _fingerID)
	{
		// Is the object that the player started touching when they first touched down on the screen equal to this game object?
		if (InputManager.main.activeObject[_fingerID.ToString()] == this.gameObject)
		{
			print("releasing amoeba");
		}
	}
}
