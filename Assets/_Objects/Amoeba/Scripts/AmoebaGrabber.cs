using UnityEngine;
using System.Collections;

public class AmoebaGrabber : MonoBehaviour
{
	
	//int fingerCount = 0;
	public AmoebaPlayerInput amoebaScript;
	
	public GameObject mStretchJoint;
	//private Vector3 mStretchJointStartPos;

	public enum GrabberStates
	{
		idle,
		stretching
	}
	
	public GrabberStates grabberCurrentState = GrabberStates.idle;
	
	
	// Use this for initialization
	void Start ()
	{
		amoebaScript = GameObject.FindGameObjectWithTag("Player").GetComponent<AmoebaPlayerInput>();
		//mStretchJointStartPos = mStretchJoint.transform.localPosition;
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		StateMachine();
		mStretchJoint.transform.position = transform.position;
	}
	
	
	void StateMachine ()
	{
		switch (grabberCurrentState)
		{
			
		case GrabberStates.idle:
			
			//this.transform.localPosition = Vector3.zero;
			
			break;
			
			
		case GrabberStates.stretching:
			//amoebaScript.currentState = AmoebaPlayerInput.AmoebaStates.aiming;
			
			break;
		}
		
	}
	
	void OnGrabberDrag (DragGesture drag)
	{ 
		amoebaScript.currentState = AmoebaPlayerInput.AmoebaStates.aiming;
		grabberCurrentState = GrabberStates.stretching;
	}
	
	void OnFingerUp (FingerUpEvent e)
	{
		if(amoebaScript.IsGrounded){
		amoebaScript.currentState = AmoebaPlayerInput.AmoebaStates.moving;
		amoebaScript.AmoebaFling(this.gameObject);
		grabberCurrentState = GrabberStates.idle;
		} 
		else
		{
			this.transform.localPosition = Vector3.zero;
		}
			
		
		
		Debug.Log("fingerup");
		//
	}
	
}
