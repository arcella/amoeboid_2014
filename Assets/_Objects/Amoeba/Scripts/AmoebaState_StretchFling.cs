﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(Collider))]

public class AmoebaState_StretchFling: AmoebaBase 
{

	
	// TODO fix rotation
	public bool useMouse = false;

	public enum SnapDir
	{
		towards,
		away
	}

	#region public variables :::::::::::::::::::::::::::
	public GameObject 	ObjectToSpawn;
	public Transform	mStretchJoint;
	public Vector3		mStretchJointStartPos;

	public float 		mMagBase = 2;
	public float 		mMagMultiplier = 5;
	public float			mDragRange = 3;
	public Vector3		mDragPlaneNormal = Vector3.up;
	public SnapDir		mSnapDirection = SnapDir.towards;
	public ForceMode 	mForceTypeToApply = ForceMode.Impulse;
	public bool			mOverrideVelocity = true;
	public bool			mPauseOnDrag;

	public Color		noForceColor = Color.yellow;
	public Color		maxForceColor = Color.red;

	#endregion

	#region private variables :::::::::::::::::::::::::::
	private Vector3		mForceVector;
	private float		mMagPercent = 0;
	private bool		mDragging = false;
	private Vector3		mTouchPos3D;
	private float		mDragDistance;
	private Plane		mDragPlane;
	private Ray			mTouchRay;
	private string		mShaderString = "Transparent/Diffuse";
	private Material	mDragZoneMat;
	private GameObject	mDragZone;

	private Vector3		adjustedPosition;

	private AmoebaController amoebaController;
	#endregion

	// Use this for initialization
	#region Start :::::::::::::::::::::::::::
	void Start () 
	{

		mStretchJointStartPos = mStretchJoint.transform.localPosition;
		amoebaController = GetComponent<AmoebaController>();

		// ANIMATION controls
		anim = mAnimationMesh.GetComponent<Animator>();

		// DragZone Creation
		Color currentColor = noForceColor;
		mDragZoneMat = new Material (Shader.Find (mShaderString));

		mDragZone = new GameObject ("dragZone_"  + gameObject.name);
		mDragZone.AddComponent<MeshFilter>().mesh = MakeDiscMeshBrute (mMagBase /4);

		// dragzone.getcomponent.meshfilter
		mDragZone.AddComponent<MeshRenderer>();
		mDragZone.renderer.enabled = false;

		mDragZone.name = "dragZone_" + gameObject.name;
		mDragZone.transform.localScale = new Vector3 (mMagBase * 2, 0.025f, mMagBase * 2);
		mDragZone.renderer.material = mDragZoneMat;
		mDragZone.renderer.material.color = currentColor * new Color (1, 1, 1, 0.2f);
		
		// create the dragplane
		mDragPlane = new Plane (mDragPlaneNormal, transform.position);
		
		// orient the dragplane
		if(mDragPlaneNormal != Vector3.zero) {
			mDragZone.transform.rotation = Quaternion.LookRotation (mDragPlaneNormal) * new Quaternion (1,0,0,1);
		} else {
			Debug.LogError ("Drag plane normal cannot be equal to Vector3.zero.");
		}
		
		// update the dragplane position
		mDragZone.transform.position = transform.position;
	}
	#endregion
	
	// ::::::::::::::::::::::::::::::::::::: UPDATE
	void Update() 
	{
		if(amoebaController.isGrounded || amoebaController.isWallSticking)
		{
			if(!useMouse)
			{
				TouchChecker();
			}

			//Debug.Log(Time.timeScale);
		} 
		else
		{
			anim.SetInteger("AnimationIndex", 0);
		}

		if(!amoebaController.isAirSwipe)
		{
			amoebaController.mAirSwipeRechargeTimer -= Time.deltaTime;
			if(amoebaController.mAirSwipeRechargeTimer <= 0)
			{
				amoebaController.ResetAirSwipe();
			}
		}
		amoebaController.AirSwipeGUI();

	}

	void TouchChecker()
	{
		#region ::::::::::::::::::::::::::::::::: touch DOWN
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
		{
			mDragging = true;
			mAnimationMesh.SetActive(false);
			mStretchMesh.SetActive(true);

			if(mPauseOnDrag)
			{
				Debug.Log("STOP TIME");
				Time.timeScale = .25f;
			}

			mDragPlane = new Plane (mDragPlaneNormal, transform.position);

			if(mDragPlaneNormal != Vector3.zero)
			{
				mDragZone.transform.rotation = Quaternion.LookRotation (mDragPlaneNormal) * new Quaternion (1, 0, 0, 1);
			} 
			else
			{
				Debug.LogError ("Drag plane normal cannot be equal to Vector3.zero.");
			}
			mDragZone.transform.position = transform.position;
			mDragZone.renderer.enabled = true;


		}
		#endregion

		#region ::::::::::::::::::::::::::::::::: dragging
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
		{
			Color CurrentColor = noForceColor;
			if(mDragPlane.GetDistanceToPoint (transform.position) != 0)
			{
				mDragPlane = new Plane (mDragPlaneNormal, transform.position);
			}

			mTouchRay = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);
			float intersectDist = 0.0f;


			if (mDragPlane.Raycast (mTouchRay, out intersectDist))
			{
				mTouchPos3D = mTouchRay.GetPoint (intersectDist);
				mDragDistance = Mathf.Clamp ((mTouchPos3D - transform.position).magnitude, 0, mMagBase);

				if (mDragDistance * mMagMultiplier < 1)
					mDragDistance = 0;

				// LIMIT DRAG OF AMOEBA TO DISTANCE
				mStretchJoint.transform.position = mTouchPos3D;

				Vector3 stretchVect = (mStretchJoint.transform.position - transform.position);
				float length = stretchVect.magnitude;

				if(length > mDragRange)
				{
					mStretchJoint.transform.position = (mStretchJoint.transform.position - transform.position).normalized * mDragRange + transform.position;
				}


				mForceVector = mTouchPos3D - transform.position;
				mForceVector.Normalize();
				mForceVector *= mDragDistance * mMagMultiplier;

				mMagPercent = (mDragDistance * mMagMultiplier) / (mMagBase * mMagMultiplier);
				CurrentColor = noForceColor * (1 - mMagPercent) + maxForceColor * mMagPercent;
				mDragZone.renderer.material.color = CurrentColor * new Color (1,1,1, 0.2f);
				Debug.DrawRay (transform.position, mForceVector / mMagMultiplier, Color.red);

			}
			mDragZone.transform.position = transform.position;

		}
		#endregion

		#region ::::::::::::::::::::::::::::::::: touch UP
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
		{
			mDragging = false;
			mAnimationMesh.SetActive(true);
			mStretchMesh.SetActive(false);
			mStretchJoint.transform.localPosition = mStretchJointStartPos;

			if (mOverrideVelocity)
			{
				rigidbody.AddForce (-rigidbody.velocity, ForceMode.VelocityChange);
			}

			int snapD = 1;

			if(mSnapDirection == SnapDir.away)
				snapD = -1;

			// FLING
			rigidbody.AddForce (snapD * mForceVector, mForceTypeToApply);
			audio.PlayOneShot(amoebaController.mFlingAudio, .2f);
			// animations
			/*
			if(mForceVector.x > 0)
			{
				anim.SetInteger("AnimationIndex", 3);
			} 
			else if (mForceVector.x < 0)
			{
				anim.SetInteger("AnimationIndex", 4);
			}
			*/
			mForceVector = Vector3.zero;
			mDragZone.renderer.enabled = false;

			if(mPauseOnDrag)
			{
				Time.timeScale = 1;
			}
		}
		#endregion
	}

	void OnSwipe(SwipeGesture gesture) 
	{
		if( (amoebaController.isGrounded == false) && (amoebaController.isWallSticking == false) && amoebaController.isAirSwipe)
		{

			rigidbody.AddForce(gesture.Move);
			amoebaController.isAirSwipe = false;
			Instantiate(amoebaController.mSwipePart, transform.position, Quaternion.LookRotation(gesture.Move, Vector3.up));
			audio.PlayOneShot(amoebaController.mAirSwipeAudio, .5f);

		}

//		Debug.Log("Swiped " 		+ gesture.Direction + 
//		          " with finger " 	+ gesture.Fingers[0] + 
//		          " (velocity:" 	+ gesture.Velocity + 
//		          ", distance: " 	+ gesture.Move.magnitude + ")");
	}


	
	void OnGUI()
	{
		if(mDragging)
		{
			//Vector2 guiCoord = GUIUtility.ScreenToGUIPoint (Input.GetTouch(0).position);
			GUI.Box (new Rect (10, Screen.height - 50, 100,20), "force: " + Mathf.Round((mForceVector).magnitude));

		}
		//GUI.Box (new Rect (10, Screen.height - 150, 100,20), "Recharge" + amoebaController.mAirSwipeRechargeTimer);

	}
	
	#region ::::::::::::::::::::::::::::::::::::::::::::::::::::::: MOUSE INPUT :::::::
	private bool 	mouseDragging = false;
	private	Ray		mouseRay;
	private	Vector3	mousePos3D;
	
	void OnMouseDown()
	{
		if(useMouse && amoebaController.isGrounded || amoebaController.isWallSticking) // IF MOUSE USE IS ACTIVE
		{
			mouseDragging = true;
			if(mPauseOnDrag){
				Time.timeScale = .25f;
			}
			
			// update the dragplane
			mDragPlane = new Plane (mDragPlaneNormal, transform.position);
			
			// orient the dragplane
			if(mDragPlaneNormal != Vector3.zero){
				mDragZone.transform.rotation = Quaternion.LookRotation (mDragPlaneNormal) * new Quaternion (1,0,0,1);
			} else
				Debug.LogError ("Drag plane normal cannot be equal to Vector3.zero.");
			
			// update the position of the dragzone
			mDragZone.transform.position = transform.position;
			mDragZone.renderer.enabled = true;
			
		}
	}
	
	void OnMouseDrag()
	{
		if(useMouse && amoebaController.isGrounded || amoebaController.isWallSticking) // IF MOUSE USE IS ACTIVE
		{
			
			Color currentColor = noForceColor;
			// update the plane if the target object has left it
			if(mDragPlane.GetDistanceToPoint (transform.position) != 0)
			{
				//update dragplane by constructing a new one
				mDragPlane = new Plane (mDragPlaneNormal, transform.position);
			}
			
			// create a ray from the camera, through the mouse position in 3d space
			mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);
			
			// if mouseRay intersects with dragplane
			float intersectDist = 0.0f;
			
			if(mDragPlane.Raycast (mouseRay, out intersectDist))
			{
				// update the world space point for the mouse position on the dragplane
				mousePos3D = mouseRay.GetPoint	(intersectDist);
				

				
				//Calculate the distance between the 3d mouse position and the object position
				mDragDistance = Mathf.Clamp((mousePos3D - transform.position).magnitude,0, mMagBase);
				
				// Calculate the force vector
				if (mDragDistance * mMagMultiplier < 1)
					mDragDistance = 0; // Allows for a "no move" buffer close to object
				
				// TODO PUT THIS INTO A FUNCTION VVVVVVVVVVV
				// CHECK IF MOUSE is BELOW

				if(mousePos3D.y < transform.position.y && amoebaController.isGrounded)
				{
					mAnimationMesh.SetActive(true);
					mStretchMesh.SetActive(false);
					anim.SetInteger("AnimationIndex", 2);
				}
				else
				{
					anim.SetInteger("AnimationIndex", 0);
					mAnimationMesh.SetActive(false);
					mStretchMesh.SetActive(true);
				}

				// LIMIT DRAG OF AMOEBA TO DISTANCE
				mStretchJoint.transform.position = mousePos3D;

				Vector3 stretchVect = (mStretchJoint.transform.position - transform.position);
				float length = stretchVect.magnitude;

				if(length > mDragRange)
				{
					mStretchJoint.transform.position = (mStretchJoint.transform.position - transform.position).normalized * mDragRange + transform.position;
				}
				//TODO PUT THIS INTO A FUNCTION ^^^^^^
				
				mForceVector = mousePos3D - transform.position;
				mForceVector.Normalize();
				mForceVector *= mDragDistance * mMagMultiplier;
				
				// UPDATE the colors
				mMagPercent = (mDragDistance * mMagMultiplier) / (mMagBase * mMagMultiplier);
				currentColor = noForceColor * (1 - mMagPercent) + maxForceColor * mMagPercent;
				mDragZone.renderer.material.color = currentColor * new Color (1, 1, 1, 0.2f);
				
				// DRAW THE LINE
				Debug.DrawRay (transform.position, mForceVector / mMagMultiplier, currentColor);
			}
			mDragZone.transform.position = transform.position;
		}
	}
	
	void OnMouseUp ()
	{
		if(useMouse && amoebaController.isGrounded || amoebaController.isWallSticking) // IF MOUSE USE IS ACTIVE
		{
			mouseDragging = false;
			
			mAnimationMesh.SetActive(true);
			mStretchMesh.SetActive(false);
			mStretchJoint.transform.localPosition = mStretchJointStartPos;
			
			if(mOverrideVelocity)
			{
				// cancel existing velocity
				rigidbody.AddForce (-rigidbody.velocity, ForceMode.VelocityChange);
			}
			
			// add new force
			int	snapD = 1;
			
			if(mSnapDirection == SnapDir.away)
				snapD = -1;
			
			rigidbody.AddForce (snapD * mForceVector, mForceTypeToApply);
			
			audio.PlayOneShot(amoebaController.mFlingAudio, .2f);
			
			// cleanup
			mDragZone.renderer.enabled = false;
			
			if(mPauseOnDrag)
				Time.timeScale = 1;
		}
	}
	#endregion
	

	#region DISK MESH 
	Mesh MakeDiscMeshBrute (float r)
	{
		
		Mesh discMesh;
		
		Vector3[] dmVerts = new Vector3[18];
		Vector3[] dmNorms = new Vector3[18];
		Vector2[] dmUVs = new Vector2[18];
		int[] dmTris = new int[48];
		
		int i = 0;
		
		
		
		discMesh = new Mesh ();

		dmVerts [0] = new Vector3 (0, 0, 0);
		dmVerts [1] = new Vector3 (0, 0, r);
		dmVerts [2] = new Vector3 (1, 0, 1).normalized * r; // find the vector at the correct distance the hacky-hillbilly way!
		dmVerts [3] = new Vector3 (r, 0, 0);
		dmVerts [4] = new Vector3 (1, 0, -1).normalized * r;
		dmVerts [5] = new Vector3 (0, 0, -r);
		dmVerts [6] = new Vector3 (-1, 0, -1).normalized * r;
		dmVerts [7] = new Vector3 (-r, 0, 0);
		dmVerts [8] = new Vector3 (-1, 0, 1).normalized * r;
		
		
		
		// set the other side to the same points
		
		for (i = 0; i<dmVerts.Length/2; i++) {
			
			dmVerts [dmVerts.Length / 2 + i] = dmVerts [i];
			
		}
		
		for (i = 0; i<dmNorms.Length; i++) {
			
			if (i < dmNorms.Length / 2)
				dmNorms [i] = Vector3.up; // set side one to face up
			
			else
				dmNorms [i] = -Vector3.up; // set side two to face down
			
		}
		
		
		
		dmUVs [0] = new Vector2 (0, 0);
		dmUVs [1] = new Vector2 (0, r);
		dmUVs [2] = new Vector2 (1, 1).normalized * r;
		;
		
		dmUVs [3] = new Vector2 (r, 0);
		dmUVs [4] = new Vector2 (1, -1).normalized * r;
		;
		
		dmUVs [5] = new Vector2 (0, -r);
		dmUVs [6] = new Vector2 (-1, -1).normalized * r;
		;
		
		dmUVs [7] = new Vector2 (-r, 0);
		dmUVs [8] = new Vector2 (-1, 1).normalized * r;
		;
		
		
		
		// set the other side to the same points
		
		for (i = 0; i<dmUVs.Length/2; i++) {
			
			dmUVs [dmUVs.Length / 2 + i] = dmUVs [i];
			
		}
		
		dmTris [0] = 0;
		dmTris [1] = 1;
		dmTris [2] = 2;
		
		dmTris [3] = 0;
		dmTris [4] = 2;
		dmTris [5] = 3;
		
		dmTris [6] = 0;
		dmTris [7] = 3;
		dmTris [8] = 4;
		
		dmTris [9] = 0;
		dmTris [10] = 4;
		dmTris [11] = 5;
		
		dmTris [12] = 0;
		dmTris [13] = 5;
		dmTris [14] = 6;
		
		dmTris [15] = 0;
		dmTris [16] = 6;
		dmTris [17] = 7;
		
		dmTris [18] = 0;
		dmTris [19] = 7;
		dmTris [20] = 8;
		
		dmTris [21] = 0;
		dmTris [22] = 8;
		dmTris [23] = 1;
		
		// side two
		dmTris [24] = 9;
		dmTris [25] = 11;
		dmTris [26] = 10;
		
		dmTris [27] = 9;
		dmTris [28] = 12;
		dmTris [29] = 11;
		
		dmTris [30] = 9;
		dmTris [31] = 13;
		dmTris [32] = 12;
		
		dmTris [33] = 9;
		dmTris [34] = 14;
		dmTris [35] = 13;
		
		dmTris [36] = 9;
		dmTris [37] = 15;
		dmTris [38] = 14;
		
		dmTris [39] = 9;
		dmTris [40] = 16;
		dmTris [41] = 15;
		
		dmTris [42] = 9;
		dmTris [43] = 17;
		dmTris [44] = 16;
		
		dmTris [45] = 9;
		dmTris [46] = 10;
		dmTris [47] = 17;
		
		discMesh.vertices = dmVerts;
		discMesh.uv = dmUVs;
		discMesh.normals = dmNorms;
		discMesh.triangles = dmTris;
		
		return discMesh;
	}
	#endregion
	
	
}
