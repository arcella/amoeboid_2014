﻿using UnityEngine;
using System.Collections;

public class CheckPointManager : MonoBehaviour 
{
	public GameObject 			mPlayer;
	//public AmoebaController 	amoebaController;
	public Controller			controller;
	public Vector3				mSpawnPosition;
	public GameObject			mCheckPointIndicator;
	public AudioClip			mReachedCheckPointAudio;


	// Use this for initialization
	void Awake () 
	{
		mPlayer	= GameObject.FindGameObjectWithTag("Player");
		controller = GameObject.FindGameObjectWithTag("Player").gameObject.GetComponentInChildren<Controller>();
		//amoebaController = GameObject.FindGameObjectWithTag("Player").GetComponent<AmoebaController>();
		mSpawnPosition = GameObject.FindGameObjectWithTag("Player").transform.position;

	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(controller.mHealth <= 0)
		{
			mPlayer.rigidbody.velocity = Vector3.zero;
			mPlayer.rigidbody.angularVelocity = Vector3.zero;
			mPlayer.transform.position = mSpawnPosition;
			controller.mHealth = 100;
		}
	}

	public void UpdateCheckPoint()
	{
		audio.PlayOneShot(mReachedCheckPointAudio);
	}

}
