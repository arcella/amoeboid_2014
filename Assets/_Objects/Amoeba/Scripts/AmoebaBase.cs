﻿using UnityEngine;
using System.Collections;

public class AmoebaBase : MonoBehaviour 
{
	// Player
	protected Transform playerTransform;
	
	// Animation
	public Animator 	anim;
	public GameObject 	mAnimationMesh;
	public GameObject	mStretchMesh;

	// health
	public  bool 		isDead;
	public int 			mHealth;

	protected virtual void Initialize() {}
	protected virtual void FSMUpdate () {}
	protected virtual void FSMFixedUpdate () {}
	protected virtual void FSMStateChange() {}
	
	void Start()
	{
		Initialize();
	}
	
	void Update()
	{
		FSMUpdate();
	}
	
	void FixedUpdate()
	{
		FSMFixedUpdate();
	}
	
	void OnDisable()
	{
		FSMStateChange();
	}

	// :::::::::::::::::::::::: ABILITIES
	public void DamageHealth(int damageAmt)
	{
		mHealth -= damageAmt;
	}


}
